********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Moderate Module
Author: Robert Castelo
Drupal: 5.x
********************************************************************
DESCRIPTION:

Enables moderation of nodes.

Which role can see nodes in moderation can be set per content type.

Views integration, so view(s) can be created to give moderation overview.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire directory of this module into your Drupal directory:
   sites/all/modules/
   

2. Enable the module by navigating to:

   administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.
  
  
********************************************************************
USAGE

1. To set permissions of who can moderate nodes of each content type navigate to:

'Administer' 
    -- 'User management'
        -- 'Access control' (admin/user/access)

   Select roles for each content type under 'moderate module'

2. On each content type there is the option to put a node in moderation when it's created

  See 'Workflow' fieldset, 'Default options', 'In moderation queue'


********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/moderate
   
- Comission New Features:
   http://drupal.org/user/3555/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F

        
********************************************************************
ACKNOWLEDGEMENT


